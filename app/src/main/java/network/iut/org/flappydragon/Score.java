package network.iut.org.flappydragon;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.NonNull;

import static android.graphics.Color.BLACK;

/**
 * Created by root on 25/04/18.
 */

public class Score implements Comparable<Score>{
    private int x,y;
    private int score;
    private Context context;
    private GameView view;
    private Paint paint;

    public Score(Context context, GameView view) {
        this.context = context;
        this.view = view;
        this.x = 10;
        this.y = 50;
        this.score = 0;
        paint = new Paint();
        paint.setColor(BLACK);
        paint.setTextSize(50);
    }

    public void implementScore() {
        score ++;
    }

    public void draw(Canvas canvas){
        canvas.drawText(String.valueOf(score) + " m", x, y, paint);
    }

    public void reset(){
        score = 0;
    }

    @Override
    public int compareTo(@NonNull Score o) {
        if (this.score < o.getScore()){
            return -1;
        }else if(this.score > o.getScore()){
            return 1;
        }else {
            return 0;
        }
    }

    public int getScore() {
        return score;
    }
}
