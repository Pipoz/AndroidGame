package network.iut.org.flappydragon;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Created by root on 26/04/18.
 */

public class Sons {
    private MediaPlayer mediaPlayer;
    private Context context;

    public Sons(Context context){
        this.context = context;
        playMain();
    }

    public void playMain(){
        mediaPlayer = MediaPlayer.create(context, R.raw.main);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
    }
    public void stopMain(){
        mediaPlayer.stop();
        mediaPlayer.release();
    }
    public void pauseMain(){
        mediaPlayer.pause();
    }
    public void playGameOver(){
        mediaPlayer = MediaPlayer.create(context, R.raw.game_over);
        mediaPlayer.setLooping(false);
        mediaPlayer.start();
    }
    public void stopGameOver(){
        mediaPlayer.stop();
        mediaPlayer.release();
    }

}
