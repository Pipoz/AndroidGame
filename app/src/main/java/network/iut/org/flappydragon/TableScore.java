package network.iut.org.flappydragon;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import java.util.ArrayList;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;
import static java.util.Collections.reverse;
import static java.util.Collections.sort;

/**
 * Created by root on 26/04/18.
 */

public class TableScore {
    private ArrayList<Integer> scores;
    private int x,y;
    private Context context;
    private GameView view;
    private Paint paint;

    public TableScore(){
        this.context = context;
        this.view = view;
        this.x = 10;
        this.y = 50;
        paint = new Paint();
        paint.setColor(WHITE);
        paint.setTextSize(60);

        //TODO Initialiser le tableau des scores avec un fichier
        scores = new ArrayList<Integer>();
    }

    public void addScore(int score){
        this.scores.add(score);
        Log.i("SCORE", "Valeurs ajoutée : " + String.valueOf(score));
        sort(this.scores);
        reverse(this.scores);
        for (int s: this.scores) {
            Log.i("SCORE", "Valeurs des scores dans le tableau : " + String.valueOf(s));
        }
        //On garde seulement les 10 meilleurs scores
        if(scores.size() > 10){
            Log.i("SCORE", "Je passe ici");
            scores.remove(9);
        }

        //TODO sauvegarder le score dans un fichier

    }

    /**
     * J'affiche ici seulement les trois premiers scores afin de ne pas surcharger l'écran de Game Over
     * @param canvas
     */
    public void drawOnGameOver(Canvas canvas){
        Log.i("SCORE","Score 0 : "  + scores.get(0));
        Rect r = new Rect();
        canvas.getClipBounds(r);
        int cHeight = r.height();
        int cWidth = r.width();
        paint.setTextAlign(Paint.Align.LEFT);
        float x = 50;
        float y = canvas.getHeight() - 400;
        canvas.drawText("Scores : ",x, y,paint);

        for(int i = 0; i < 3; i++){
            if(i < scores.size()){
                int s = this.scores.get(i);
                String toPrint = i+1 + ". " + s;
                paint.getTextBounds(toPrint, 0, toPrint.length(), r);
                canvas.drawText(toPrint,x, y + (i+1)*100,paint);
            }
        }
    }
}
