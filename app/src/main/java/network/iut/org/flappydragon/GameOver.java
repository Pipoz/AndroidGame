package network.iut.org.flappydragon;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import static android.graphics.Color.BLACK;
import static network.iut.org.flappydragon.Util.*;

public class GameOver {
    private int x;
    private int y;
    private boolean perdu;
    private int heightR, widthR, heightG, widthG;
    private GameView view;
    private Bitmap gO, retry;
    private Context context;


    public GameOver(Context context, GameView view) {
        this.context = context;
        widthG = 1000;
        heightG = 691;
        gO = decodeSampledBitmapFromResource(context.getResources(), R.drawable.game_over, widthG, heightG);
        widthR = 200;
        heightR = 210;
        retry = decodeSampledBitmapFromResource(context.getResources(), R.drawable.retry, widthR, heightR);
        this.view = view;
        this.perdu = false;
    }

    public void draw(Canvas canvas) {
        Paint myPaint = new Paint();
        myPaint.setColor(BLACK);
        myPaint.setStrokeWidth(10);
        int xG = context.getResources().getDisplayMetrics().widthPixels/2 - widthG/2;
        int xR = canvas.getWidth()/2 - widthR/2;
        canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), myPaint);
        canvas.drawBitmap(gO,null, new Rect(xG,0,x + widthG , heightG), null);
        canvas.drawBitmap(retry,null, new Rect(xR, heightG + 50 , xR + widthR, heightG + 50 + heightR), null);


    }

    public boolean aPerdu(){
        return perdu;
    }

    public void setPerdu(boolean perdu){
        this.perdu = perdu;
    }

    public GameView getView() {
        return view;
    }
}