package network.iut.org.flappydragon;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

public class GameView extends SurfaceView implements Runnable {
    public static final long UPDATE_INTERVAL = 50; // = 20 FPS
    private SurfaceHolder holder;
    private boolean paused = true;
    private Timer timer = new Timer();
    private TimerTask timerTask;
    private Player player;
    private Obstacle obstacle;
    private Background background;
    private Score score;
    private TableScore tableScore;
    private int xObstacle;
    private int yObstacle;
    private GameOver gameOver;
    private Pause pause;
    private Sons sons;

    public GameView(Context context) {
        super(context);
        player = new Player(context, this);
        background = new Background(context, this);
        pause = new Pause(context, this);
        obstacle = new Obstacle(context,this);
        gameOver = new GameOver(context, this);
        score = new Score(context, this);
        tableScore = new TableScore();
        sons = new Sons(context);
        holder = getHolder();
        new Thread(new Runnable() {
            @Override
            public void run() {
                GameView.this.run();
            }
        }).start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        performClick();
        int xTouch = Float.valueOf(event.getX()).intValue();
        int yTouch = Float.valueOf(event.getY()).intValue();

        Log.i("PAUSE", String.format("%d %d", xTouch, yTouch));
        Log.i("PAUSE", String.format("%d %d %d %d", pause.getRect().left, pause.getRect().right, pause.getRect().top, pause.getRect().bottom));
        if(gameOver.aPerdu()){
            resume();
        }
        if (pause.getRect().contains(xTouch, yTouch)){
            paused = true;
            Log.i("PAUSE", "PAUSE TRUE");
        }else if(event.getAction() == MotionEvent.ACTION_DOWN){
            if(paused) {
                resume();
            } else {
                Log.i("PLAYER", "PLAYER TAPPED");
                this.player.onTap();
            }
        }
        return true;
    }

    private void resume() {
        if(paused){
            paused = false;
        }
        if(gameOver.aPerdu()){
            gameOver.setPerdu(false);
            sons.stopGameOver();
        }
        sons.playMain();
        startTimer();
    }

    private void startTimer() {
        Log.i("TIMER", "START TIMER");
        setUpTimerTask();
        timer = new Timer();
        timer.schedule(timerTask, UPDATE_INTERVAL, UPDATE_INTERVAL);
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }
        if (timerTask != null) {
            timerTask.cancel();
        }
    }

    private void setUpTimerTask() {
        stopTimer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                GameView.this.run();
            }
        };
    }

    @Override
    public void run() {
        player.move();
        draw();
    }

    private void draw() {
        while(!holder.getSurface().isValid()){
			/*wait*/
            try { Thread.sleep(10); } catch (InterruptedException e) { e.printStackTrace(); }
        }
        Canvas canvas = holder.lockCanvas();
        if (canvas != null) {
            drawCanvas(canvas);
        }
        try {
            holder.unlockCanvasAndPost(canvas);
        } catch (IllegalStateException e) {
            // Already unlocked
        }
    }

    private void drawCanvas(Canvas canvas) {
        background.draw(canvas);
        player.draw(canvas);
        pause.draw(canvas);
        obstacle.draw(canvas);
        score.implementScore();
        score.draw(canvas);
        if(player.getRect().intersect(obstacle.getRect())){
            sons.stopMain();
            sons.playGameOver();
            gameOver.setPerdu(true);
            stopTimer();
            player.resetPlayer();
            obstacle.reset();
            tableScore.addScore(score.getScore());
            gameOver.draw(canvas);
            tableScore.drawOnGameOver(canvas);
            score.reset();

        }
        if (paused) {
            sons.pauseMain();
            pause.drawEnPause(canvas);
            stopTimer();
        }
    }

}
