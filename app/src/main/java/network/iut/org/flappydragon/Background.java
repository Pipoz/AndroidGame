package network.iut.org.flappydragon;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

import java.util.Timer;

import static network.iut.org.flappydragon.Util.*;

public class Background {
    private final int imageW;
    private int height;
    private int width;
    private GameView view;
    private Bitmap background1;
    private Bitmap background2;
    private Bitmap background3;
    private Bitmap background4;
    private Bitmap background5;
    private int xBG1;
    private int xBG2;
    private int speed = 32;

    public Background(Context context, GameView view) {
        height = context.getResources().getDisplayMetrics().heightPixels;
        width = context.getResources().getDisplayMetrics().widthPixels;
        background1 = decodeSampledBitmapFromResource(context.getResources(), R.drawable.layer1, width, height);
        background2 = decodeSampledBitmapFromResource(context.getResources(), R.drawable.layer2, width, height);
        background3 = decodeSampledBitmapFromResource(context.getResources(), R.drawable.layer3, width, height);
        background4 = decodeSampledBitmapFromResource(context.getResources(), R.drawable.layer4, width, height);
        background5 = decodeSampledBitmapFromResource(context.getResources(), R.drawable.layer5, width, height);
        
        imageW = background1.getWidth() * width /height;
        xBG2 = imageW;
        xBG1 = 0;
        this.view = view;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(background1, null, new Rect(xBG1,0, xBG1 + imageW, height), null);
        canvas.drawBitmap(background2, null, new Rect(xBG1,0, xBG1 + imageW, height), null);
        canvas.drawBitmap(background3, null, new Rect(xBG1,0, xBG1 + imageW, height), null);
        canvas.drawBitmap(background4, null, new Rect(xBG1,0, xBG1 + imageW, height), null);
        canvas.drawBitmap(background5, null, new Rect(xBG1,0, xBG1 + imageW, height), null);

        canvas.drawBitmap(background1, null, new Rect(xBG2,0, xBG2 + imageW, height), null);
        canvas.drawBitmap(background2, null, new Rect(xBG2,0, xBG2 + imageW, height), null);
        canvas.drawBitmap(background3, null, new Rect(xBG2,0, xBG2 + imageW, height), null);
        canvas.drawBitmap(background4, null, new Rect(xBG2,0, xBG2 + imageW, height), null);
        canvas.drawBitmap(background5, null, new Rect(xBG2,0, xBG2 + imageW, height), null);

        if(xBG1 + imageW <= speed ){
            Log.i("Retour", "retour de BG 1 x = " + xBG1);
            Log.i("Retour", "image width " + imageW);

            xBG1 = imageW;

        }
        if(xBG2 + imageW <= speed){
            Log.i("Retour retour de BG 2 ", "x = " + xBG1);
            xBG2 = imageW;

        }
        xBG1 -= speed;
        xBG2 -= speed;
        int droite1 = xBG1 + imageW ;
        int droite2 = xBG2 + imageW ;
    }
}