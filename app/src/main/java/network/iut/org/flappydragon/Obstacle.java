package network.iut.org.flappydragon;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

import java.util.Timer;

import static network.iut.org.flappydragon.Util.*;

public class Obstacle {
    private int x = 0;
    private int y;
    private int height, width;
    private int speed = 60;
    private GameView view;
    private Bitmap obstacle;
    private Context context;
    private Canvas canvas;
    private final byte frameTime;
    private int frameTimeCounter;


    public Obstacle(Context context, GameView view) {
        this.context = context;
        height = 100;
        width = 100;
        obstacle = decodeSampledBitmapFromResource(context.getResources(), R.drawable.missile, width, height);
        this.frameTime = 3;		// the frame will change every 3 runs
        this.view = view;
        x  = context.getResources().getDisplayMetrics().widthPixels;
    }

    public void draw(Canvas canvas) {

        if(x < 0){
            int yMax = context.getResources().getDisplayMetrics().heightPixels;
            this.y = (int)(Math.random() * (yMax - 50) - 50);
            this.x = context.getResources().getDisplayMetrics().widthPixels;
        }


        Log.i("OBSTACLE","Coordonnée de l'obstacle : " + y + " , " + x);
        this.canvas = canvas;
        canvas.drawBitmap(obstacle, x , y, null);
        x -= speed;
    }


    protected void changeToNextFrame(){
        this.frameTimeCounter++;
        if(this.frameTimeCounter >= this.frameTime){
            //TODO Change frame
            this.frameTimeCounter = 0;
        }
    }

    private float getSpeedTimeDecrease() {
        return view.getHeight() / 320;
    }

    private float getMaxSpeed() {
        return view.getHeight() / 51.2f;
    }

    public Rect getRect() {
        return new Rect(x, y, x + width, y+height);
    }

    public GameView getView() {
        return view;
    }

    public void reset(){
        this.x = context.getResources().getDisplayMetrics().widthPixels;
    }
}