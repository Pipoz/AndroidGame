package network.iut.org.flappydragon;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import static android.graphics.Color.BLACK;
import static network.iut.org.flappydragon.Util.decodeSampledBitmapFromResource;

/**
 * Created by root on 25/04/18.
 */

public class Pause {
    private String pause = "On pause";
    private String instruction = "Tap to play !";
    private Paint paint;
    private Rect rect;
    private Bitmap bitmap;
    private Context context;
    private GameView view;

    public Pause(Context context, GameView view) {
        this.context = context;
        this.view = view;
        paint = new Paint();
        paint.setColor(BLACK);
        paint.setTextSize(100);
        bitmap = decodeSampledBitmapFromResource(context.getResources(), R.drawable.pause, 50, 50);

    }

    public void draw(Canvas canvas){
        rect = new Rect(900,0,canvas.getWidth(), 100);
        int x,y;
        y = 10;
        x = context.getResources().getDisplayMetrics().widthPixels - 130;
        canvas.drawBitmap(bitmap, x , y, null);
    }

    public void drawEnPause(Canvas canvas){
        Rect r = new Rect();
        canvas.getClipBounds(r);
        int cHeight = r.height();
        int cWidth = r.width();
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(pause, 0, pause.length(), r);
        float x = cWidth / 2f - r.width() / 2f - r.left;
        float y = 400;
        canvas.drawText(pause, x, y, paint);
        paint.setTextSize(80);
        canvas.drawText(instruction, x, y + 200, paint);
    }

    public Rect getRect() {
        return rect;
    }
}
