package network.iut.org.flappydragon;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

import java.util.Timer;

import static network.iut.org.flappydragon.Util.*;

public class Menu {
    private int x;
    private int y;
    private int height, width;
    private GameView view;
    private Bitmap menu;
    private Context context;


    public Menu(Context context, GameView view) {
        this.context = context;
        menu = decodeSampledBitmapFromResource(context.getResources(), R.drawable.pause, 50, 50);
        height = 50;
        width = 50;
        this.view = view;
    }

    public void draw(Canvas canvas) {
         y = 10;
         x = context.getResources().getDisplayMetrics().widthPixels - 130;
        canvas.drawBitmap(menu, x , y, null);

    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getHeight() {
        return height;
    }

    public float getWidth() {
        return width;
    }

    public Rect getRect() {
        return new Rect(x, y, x + width, y+height);
    }

    public GameView getView() {
        return view;
    }
}